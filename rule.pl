is_loop(Event,Guard):- transition(State,State,Event,Guard,_).
all_loops(Set) :- findall(Event, is_loop(Event,_), Set).
is_edge(Event, Guard):- transition(_,_,Event,Guard,_).
size(Length) :- findall(_, transition(_,_,_,_,_), L), length(L, Length).
is_link(Event,Guard) :- transition(Source,Destination,Event,Guard,_), Source \= Destination.
all_superstates(Set) :- findall(Event, superstate(Event,_), L), list_to_set(L, Set).
ancestor(Ancestor, Descendant) :- transition(Ancestor,Descendant,_,_,_).
inherits_transitions(State, List) :- superstate(Superstate, State), findall(Event, transition(Superstate,_,Event,_,_), List).
all_states(L) :- findall(State, state(State), L).
all_init_states(L) :- findall(State, initial_state(State,_), L).
get_starting_state(State) :- initial_state(State,null).
state_is_reflexive(State) :- transition(State,State,_,_,_).
graph_is_reflexive :- get_starting_state(State), transition(_,State,_,_,_).
get_guards(Ret) :- findall(Guard, (transition(_,_,_,Guard,_), Guard \= null), L), list_to_set(L,Ret).
get_events(Ret) :- findall(Event, (transition(_,_,Event,_,_), Event \= null), L), list_to_set(L,Ret).
get_actions(Ret) :- findall(Action, (transition(_,_,_,_,Action),Action \= null), L), list_to_set(L,Ret).
get_only_guarded(Ret) :- findall([Source,Destination], (transition(Source,Destination,_,Guard,_),Guard \= null), L), list_to_set(L,Ret).
legal_events_of(State,L) :- findall([Event,Guard], (transition(_,State,Event,Guard,_);transition(State,_,Event,Guard,_)), List), list_to_set(List,L).