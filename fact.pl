/*listing states*/
state(dormant).
state(init).
state(idle).
state(error_diagnosis).
state(safe_shutdown).
state(monitoring).
state(boot_hw).
state(senchk).
state(tchk).
state(psichk).
state(ready).
state(monidle).
state(regulate_environment).
state(lockdown).
state(prep_vpurge).
state(initiate_purge).
state(alt_temp).
state(alt_psi).
state(risk_assess).
state(safe_status).
state(error_rcv).
state(applicable_rescue).
state(reset_module_data_state).
/*listing all initial states*/
initial_state(dormant,null).
initial_state(boot_hw,init).
initial_state(monidle,monitor).
initial_state(prep_vpurge,lockdown).
initial_state(error_rcv,error_diagnosis).
/*listing superstates*/
superstate(init,boot_hw).
superstate(init,senchk).
superstate(init,tchk).
superstate(init,psichk).
superstate(init,ready).
superstate(monitor,monidle).
superstate(monitor,regulate_environment).
superstate(monitor,lockdown).
superstate(lockdown,prep_vpurge).
superstate(lockdown,initiate_purge).
superstate(lockdown,alt_temp).
superstate(lockdown,alt_psi).
superstate(lockdown,risk_assess).
superstate(lockdown,safe_status).
superstate(error_diagnosis,error_rcv).
superstate(error_diagnosis,applicable_rescue).
superstate(error_diagnosis,reset_module_data_state).
/*listing transitions for superstates*/
transition(null,dormant,null,null,null).
transition(dormant,null,kill,null,null).
transition(init,idle,init_ok,null,null).
transition(idle,monitoring,begin_monitoring,null,null).
transition(init,error_diagnosis,init_crash,null,'init_err_msg').
transition(error_diagnosis,init,init_retry,'retry <= 3','retry++').
transition(error_diagnosis,safe_shutdown,shutdown,'retry>3',null).
transition(safe_shutdown,dormant, sleep, null, null).
transition(idle,error_diagnosis,idle_crash,null,'idle_err_msg').
transition(error_diagnosis,idle,idle_rescue,null,null).
transition(monitoring,error_diagnosis,monitor_crash,null,'moni_err_msg').
transition(error_diagnosis,monitoring,moni_rescue,null,null).
/*listing transitions for states*/
transition(boot_hw,senchk,hw_ok,null,null).
transition(senchk,tchk,nenok,null,null).
transition(tchk,psichk,t_ok,null,null).
transition(psichk,ready,psi_ok,null,null).
transition(monidle,regulate_environment,co_contagion,null,null).
transition(regulate_environment,monidle,after_100ms,null,null).
transition(regulate_environment,lockdown,contagion_alert,null,'FACILITY_CRIT_MESG').
transition(lockdown,monidle,purge_succ,null,null).
transition(prep_vpurge,alt_temp,initiate_purge,null,'lock_doors').
transition(prep_vpurge,alt_psi,initiate_purge,null,'lock_doors').
transition(alt_temp,risk_assess,'tcyc_comp;psicyc_comp',null,null).
transition(altpsi,risk_assess,'psicyc_comp;tcyc_comp',null,null).
transition(risk_assess,prep_vpurge,null,'risk>1%',null).
transition(risk_assess,safe_status,null,'risk<=1%','unlock_doors').
transition(error_rcv,applicable_rescue,apply_protocol_rescues,'err_protocol_def=true',null).
transition(error_rcv,reset_data_state,reset_to_stable,'err_protocol_def=false',null).